package kz.iitu.PaymentSystem.service;
import kz.iitu.PaymentSystem.entity.Sale;
import kz.iitu.PaymentSystem.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class SaleService {

    @Autowired
    private SaleRepository saleRepository;

    @Transactional
    public void createSale(Sale sale) {
        saleRepository.save(sale);
    }

    @Transactional
    public List getAllSales() {
        return (List<Sale>) saleRepository.findAll();
    }

    @Transactional
    public Sale getSale(int id) {
        return saleRepository.findSaleById(id);
    }

}
