package kz.iitu.PaymentSystem;
import kz.iitu.PaymentSystem.service.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Scanner;

@Component
public class RunApp {
    Scanner in = new Scanner(System.in);

    @Autowired
    private Test test;

    public void runApp(){
        int choice = 0;
        while(choice != 4){
            System.out.println(" Choose the option by using number! ");
            System.out.println(" 1.Create a sale");
            System.out.println(" 2.Create a employee" );
            System.out.println(" 3.Calculate salary");
            System.out.println(" 4.Exit");
            choice = in.nextInt();

            switch (choice) {

                case 1:
                    test.createSale();
                    break;
                case 2:
                    test.createEmployee();
                    break;
                case 3:
                    test.calculateSalary();
                    break;
                case 4:
                    System.out.println(" Exit ");
                    System.exit(0);
            }
        }
    }
}
