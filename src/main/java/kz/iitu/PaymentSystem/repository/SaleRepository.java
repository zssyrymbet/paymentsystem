package kz.iitu.PaymentSystem.repository;
import kz.iitu.PaymentSystem.entity.Sale;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface SaleRepository extends CrudRepository<Sale, Long> {
    Sale findSaleById(int id);
}
