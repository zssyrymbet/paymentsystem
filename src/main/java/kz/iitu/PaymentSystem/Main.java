package kz.iitu.PaymentSystem;
import kz.iitu.PaymentSystem.config.SpringConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        RunApp company = context.getBean("runApp", RunApp.class);
        company.runApp();
    }

}
