package kz.iitu.PaymentSystem.event.handler;
import kz.iitu.PaymentSystem.event.EmployeeUpdateSalaryEvent;
import org.springframework.context.ApplicationListener;

public class EmployeeUpdateSalaryHandler implements ApplicationListener<EmployeeUpdateSalaryEvent> {

    @Override
    public void onApplicationEvent(EmployeeUpdateSalaryEvent employeeUpdateSalaryEvent) {
        System.out.println("EmployeeUpdateSalaryEvent");
        employeeUpdateSalaryEvent.updateSalary();
    }
}
