package kz.iitu.PaymentSystem.event;

import kz.iitu.PaymentSystem.entity.Employee;
import kz.iitu.PaymentSystem.service.Test;
import org.springframework.context.ApplicationEvent;

public class EmployeeUpdateSalaryEvent extends  ApplicationEvent{
    private int ID;
    private double salary;
    private Test test;

    public EmployeeUpdateSalaryEvent(Object source, int ID, double salary) {
        super(source);
        this.ID = ID;
        this.salary = salary;
    }

    public void updateSalary(){
       test.calculateSalary();
    }
}
