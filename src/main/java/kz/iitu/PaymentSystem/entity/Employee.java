package kz.iitu.PaymentSystem.entity;
import javax.persistence.Id;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "employees")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private double numberOfHours;
    private double salary;
    private double hourRate;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="employee_id")
    private List<Sale> sales;

    @Enumerated(EnumType.STRING)
    private EmployeeType employeeType;

    public Employee(String name, double numberOfHours, double salary,
                    double hourRate, List<Sale> sales, EmployeeType employeeType) {
        this.name = name;
        this.numberOfHours = numberOfHours;
        this.salary = salary;
        this.hourRate = hourRate;
        this.sales = sales;
        this.employeeType = employeeType;
    }

    public Employee() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(double numberOfHours) {
        this.numberOfHours = numberOfHours;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getHourRate() {
        return hourRate;
    }

    public void setHourRate(double hourRate) {
        this.hourRate = hourRate;
    }

    public List<Sale> getSales() {
        return sales;
    }

    public void setSales(List<Sale> sales) {
        this.sales = sales;
    }

    public EmployeeType getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(EmployeeType employeeType) {
        this.employeeType = employeeType;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", numberOfHours=" + numberOfHours +
                ", salary=" + salary +
                ", hourRate=" + hourRate +
                ", sales=" + sales +
                ", employeeType=" + employeeType +
                '}';
    }
}
