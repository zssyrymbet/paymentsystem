package kz.iitu.PaymentSystem.entity;

public enum EmployeeType {
    SALARIED,
    HOURLY,
    COMMISSION,
    SALARIED_COMMISSION
}
