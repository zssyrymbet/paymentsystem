package kz.iitu.PaymentSystem.service;
import kz.iitu.PaymentSystem.entity.Employee;
import kz.iitu.PaymentSystem.event.EmployeeUpdateSalaryEvent;
import kz.iitu.PaymentSystem.repository.EmployeeRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class EmployeeService implements ApplicationEventPublisherAware {
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Transactional
    public void createEmployee(Employee employee) {
        employeeRepository.save(employee);
    }

    @Transactional
    public List getAllEmployees() {
        return (List<Employee>) employeeRepository.findAll();
    }

    @Transactional
    public Employee getEmployee(int id) {
        return employeeRepository.findEmployeeById(id);
    }

    @Transactional
    public void updateEmployee(int id, double salary) {
        Employee updateEmployee = employeeRepository.findEmployeeById(id);
        updateEmployee.setSalary(salary);
        employeeRepository.save(updateEmployee);
        System.out.println("You updated employee!");
        this.eventPublisher.publishEvent(new EmployeeUpdateSalaryEvent(this, id, salary));
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.eventPublisher = applicationEventPublisher;
    }
}
