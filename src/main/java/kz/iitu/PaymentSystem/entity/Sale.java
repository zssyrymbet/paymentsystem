package kz.iitu.PaymentSystem.entity;
import javax.persistence.*;

@Entity
@Table(name="sales")
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private double price;
    private double benefit;


    public Sale(String name, double price, double benefit) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.benefit = benefit;
    }

    public Sale(int id) {
        this.id = id;
    }

    protected Sale() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getBenefit() {
        return benefit;
    }

    public void setBenefit(double benefit) {
        this.benefit = benefit;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", benefit=" + benefit +
                '}';
    }
}
