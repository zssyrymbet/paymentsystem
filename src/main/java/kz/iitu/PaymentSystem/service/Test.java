package kz.iitu.PaymentSystem.service;
import kz.iitu.PaymentSystem.entity.Employee;
import kz.iitu.PaymentSystem.entity.EmployeeType;
import kz.iitu.PaymentSystem.entity.Sale;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
public class Test {
    Scanner in = new Scanner(System.in);
    ArrayList<Employee> employees = new ArrayList<Employee>();

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private SaleService saleService;

    public void createEmployee(){
        System.out.println("Enter your name: ");
        String name = in.next();
        System.out.println("Enter number of hours you worked: ");
        double numberOfHours = in.nextDouble();
        System.out.println("Enter rate for hour: ");
        double hourRate = in.nextDouble();
        System.out.println("Enter your type: ");
        System.out.println("SALARIED");
        System.out.println("HOURLY");
        System.out.println("COMMISSION");
        System.out.println("SALARIED_COMMISSION");
        ArrayList<Sale> sales = new ArrayList<>();
        int employeeType = in.nextInt();
        EmployeeType employee_type = null;
            switch (employeeType) {
                case 1:
                    employee_type = EmployeeType.SALARIED;
                    break;
                case 2:
                    employee_type = EmployeeType.HOURLY;
                    break;
                case 3:
                    employee_type = EmployeeType.COMMISSION;
                    System.out.println("Do you have sales?: Yes ot No");
                    String answer = in.next();
                    if(answer.equals("Yes")){
                        saleService.getAllSales();
                        System.out.println("Enter ID of sale:");
                        int id = in.nextInt();
                        sales.add(saleService.getSale(id));
                    }
                    break;
                default:
                    employee_type = EmployeeType.SALARIED_COMMISSION;
                    System.out.println("Do you have sales?: Yes ot No");
                    String answerType = in.next();
                    if(answerType.equals("Yes")){
                        saleService.getAllSales();
                        System.out.println("Enter ID of sale:");
                        int id = in.nextInt();
                        sales.add(saleService.getSale(id));
                    }
                    break;
            }

        Employee employee = new Employee(name,numberOfHours,0,hourRate, sales, employee_type);
        employeeService.createEmployee(employee);
    }

    public void createSale(){
        saleService.getAllSales();
        System.out.println("Enter sale name: ");
        String name = in.next();
        System.out.println("Enter sale price: ");
        double price = in.nextDouble();
        System.out.println("Enter sale benefit: LIKE 10 or 20" );
        double benefit = in.nextDouble();
        Sale sale = new Sale(name,price,benefit);
        saleService.createSale(sale);
    }

    public void calculateSalary(){
        Hibernate.initialize(employeeService.getAllEmployees());
        System.out.println("Enter your ID: ");
        int ID = in.nextInt();
        double salary = 0;
        Employee employee = employeeService.getEmployee(ID);
        System.out.println(employee);
        switch (employee.getEmployeeType()){
            case HOURLY:
                System.out.println("Hourly");
                if(employee.getNumberOfHours() > 40){
                    double overWork = employee.getNumberOfHours() - 40;
                    salary = (employee.getNumberOfHours() * employee.getHourRate()) +
                            (employee.getHourRate() * 1.5 * overWork);
                    System.out.println("Salary" + salary);
                }else{
                    salary = employee.getNumberOfHours() * employee.getHourRate();
                }
                break;
            case COMMISSION:
                System.out.println("Commission");
                employees.add(employeeService.getEmployee(ID));
                for (int i = 0; i < employees.size(); i++){
                    if(employees.get(i).getSales() != null) {
                        for (int j = 0; j < employees.get(i).getSales().size(); j++) {
                            salary = salary + employee.getSales().get(j).getPrice() * (1 - employee.getSales().get(j).getBenefit() / 100);
                        }
                    }else{
                            salary = salary + 0;
                        }
                }
                break;
            case SALARIED_COMMISSION:
                System.out.println("Salaried-commission");
                salary = employee.getNumberOfHours() * employee.getHourRate();
                employees.add(employeeService.getEmployee(ID));
                for (int i = 0; i < employees.size(); i++){
                    if(employees.get(i).getSales() != null){
                        for (int j = 0; j < employees.get(i).getSales().size(); j++) {
                            salary = salary + employee.getSales().get(j).getPrice() * (1 - employee.getSales().get(j).getBenefit() / 100);
                        }
                    } else{
                        salary = salary + 0;
                    }
                }
                break;
            default:
                System.out.println("Salaried");
                salary = employee.getNumberOfHours() * employee.getHourRate();
        }
        employeeService.updateEmployee(ID,salary);
        System.out.println( employeeService.getEmployee(ID));

    }
}
